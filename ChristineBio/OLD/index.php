<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Christine's Biography</title>

    <img src="images/14932457-illustration-abstract-background-with-blue.jpg" class="bg">
  
    <link href="../styles/ChristineBio.css" rel="stylesheet" type="text/css" />
    <link href="contact_form.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.js"></script>
    <script src="contact_form.js"></script>
    <script type="text/javascript" src="Scripts/ChristineBio.js"></script>
    
          <nav class="fixed-nav-bar">
  <!-- Fixed navigation bar content -->

  <!-- the navigation links -->
    <div "navbar">
     <ul id="navigation">
        <!-- <li><img src="images/ChristineKnezevicPortrait.jpg" class="smallResize" "superSmallPic"> </li> -->
        <li><a href="../CK_Bio.html"</a>About Me</li>
        <li><a href="../CK_Finance.html">Finance</a></li>
        <li><a href="../CK_Technology.html">Technology</a></li>
        <li><a href="../CK_Realtor.html">Real Estate</a></li>
     </ul>
    </div>
  </nav>
    
</head>  
  <div id="mainform">

      <!-- Required Div Starts Here -->
      <form id="form">
    <h3>Contact Form</h3>
      <p id="returnmessage"></p>
      <label>Name: <span>*</span></label>
      <input type="text" id="name" placeholder="Name"/>
      <label>Email: <span>*</span></label>
      <input type="text" id="email" placeholder="Email"/>
      <label>Contact No: <span>*</span></label>
      <input type="text" id="contact" placeholder="10 digit Mobile no."/>
      <label>Message:</label>
      <textarea id="message" placeholder="Message......."></textarea>
      <input type="button" id="submit" value="Send Message"/>
      </form>
  </div>
  

</body>

</html>
