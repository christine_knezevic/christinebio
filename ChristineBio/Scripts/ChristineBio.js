

$(".circle").on("mouseover", function() {
  $(this).css("background", "#B8B8B8");
});

$(".circle").on("mouseout", function() {
  var theCircle = $(this);

  if (theCircle.hasClass("odd")) {
    theCircle.css("background", "#3399FF");
  } else {
    theCircle.css("background", "white");
  }
});

$(document).ready(function(){
  $(".accordion p").hide();
  $(".accordion h3").click(function(){
    $(this).next("p").slideToggle("slow")
     .siblings("p:visible").slideUp("slow");
    $(this).toggleClass("active");
    $(this).siblings("h3").removeClass("active");
  });
});